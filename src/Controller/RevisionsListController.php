<?php

namespace Drupal\referenced_entities_revision\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
/**
 * Class RevisionsListController.
 */
class RevisionsListController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * Revisionlist.
   *
   * @return array
   */
  public function revisionList($entity = NULL,&$referenced_entities_list = []) {
    // Field types that can reference other entities.
    $field_types = [
      'entity_reference_revisions',
      'entity_reference',
    ];

    // Referencable entities that we want to include.
    $target_types = [
      'node',
    ];

    $includes = [];
    if($entity == NULL)
      $entity = \Drupal\node\Entity\Node::load('471');
    // Iterate the fields of an entity and look for fields that can reference
    // other entities.
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    foreach ($field_definitions as $field_definition) {
      if (!$field_definition->getFieldStorageDefinition()->isBaseField()) {
        // Only include specific field types.
        if (in_array($field_definition->getType(), $field_types)) {
          // Only include specific entities.
          if (in_array($field_definition->getSetting('target_type'), $target_types)) {
            $includes[] = $field_definition->getName();
            // Get the referenced entities and also get their relationships.
            $referenced_entities = $entity->get($field_definition->getName())->referencedEntities();
            foreach ($referenced_entities as $referenced_entity) {
              $referenced_entities_list[] = $referenced_entity->id();
              $_includes = $this->revisionList($referenced_entity,$referenced_entities_list);
              foreach ($_includes as $_include) {
                $full_path = $field_definition->getName() . '.' . $_include;
                if (!in_array($full_path, $includes)) {
                  $includes[] = $full_path;
                }
              }
            }
          }
        }
      }
    }
    
    return($referenced_entities_list);
  }
  public function listRender($node){    
    $referenced_entities = [];
    $referenced_entities[] = $node->id();
    $this->revisionList($node, $referenced_entities);
    $all_revisions = [];
    foreach($referenced_entities as $entity_id){
      $entity = \Drupal\node\Entity\Node::load($entity_id);
      $vids = \Drupal::entityTypeManager()->getStorage('node')->revisionIds($entity);
      foreach($vids as $vid){        
        $revision = \Drupal::entityTypeManager()->getStorage('node')->loadRevision($vid);
        $revert_url = Url::fromRoute('node.revision_revert_confirm', array('node' => $entity_id, 'node_revision' => $revision->get('vid')->value));
        $owner = $revision->getRevisionUser();
        
        $all_revisions[$revision->get('vid')->value] = [
          'name' => $revision->get('title')->value,
          'layout_type' => $revision->type->entity->label(),
          'creator' => $owner->getDisplayName(),
          'node_id' => $revision->get('nid')->value,
          'version_id' => $revision->get('vid')->value,
          
          'changed' => date('Y/m/d H:i:s',$revision->get('changed')->value),
          'edit_url' => Link::fromTextAndUrl('View',$revision->toUrl('revision'))->toString(),
          'revert_url' => Link::fromTextAndUrl('Revert',$revert_url)->toString(),
        ];
      }
    }
    $date = array_column($all_revisions, 'changed');
    array_multisort($date, SORT_DESC, $all_revisions);
    $rows = $all_revisions;
    $header = [
      'name' => t('Title'),
      'layout_type' => t('Item type'),
      'creator' => t('Creator'),
      'node_id' => t('NID'),
      'version_id' => t('Revision ID'),      
      'changed' => t('Changed'),
      'edit_url' => t('View'),
      'revert_url'  => t('Revert'),
    ];
    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
    ];
    return [
      '#type' => '#markup',
      '#markup' => render($build)
    ];

  }

}
